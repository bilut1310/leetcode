from collections import deque
from typing import Deque

# Definition for singly-linked list.
class ListNode:
    '''Singly-linked list'''

    def __init__(self, x):
        self.val = x
        self.next = None

    def __repr__(self):
        return f'<ListNode: {self}>'

    def __str__(self):
        s = f'{self.val}'
        node = self
        while node.next is not None:
            node = node.next
            s += f'->{node.val}'
        return s

    @classmethod
    def from_str(cls, string: str, sep='->'):
        values = string.split(sep)
        head = cls(values[0])
        pnode = head
        for val in values[1:]:
            node = cls(val)
            pnode.next = node
            pnode = node
        return head


# Definition for a binary tree node.
class TreeNode:
    '''Binary tree node'''

    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

    def __repr__(self):
        return f'<TreeNode: {self}>'

    def __str__(self):
        if self.right is None:
            if self.left is None:
                val = ''
            else:
                val = f'({self.left})'
        else:
            if self.left is None:
                val = f'()({self.right})'
            else:
                val = f'({self.left})({self.right})'
        return f'{self.val}{val}'

    def to_array(self):
        arr = []
        node_queue = deque([self])
        valid_queue_size = 1
        while valid_queue_size > 0:
            node = node_queue.popleft()  # type: TreeNode
            if node is None:
                arr.append(None)
            else:
                valid_queue_size -= 1
                arr.append(node.val)
                if node.left:
                    node_queue.append(node.left)
                    valid_queue_size += 1
                else:
                    node_queue.append(None)
                if node.right:
                    node_queue.append(node.right)
                    valid_queue_size += 1
                else:
                    node_queue.append(None)
        return arr

    @classmethod
    def from_array(cls, arr: list):
        i = 1
        root = cls(arr[0])
        node_queue = deque([root])
        while i < len(arr):
            curr_node = node_queue.popleft()  # type: TreeNode
            if arr[i] is not None:
                node = cls(arr[i])
                curr_node.left = node
                node_queue.append(node)
            i += 1

            if i >= len(arr):
                break
            if arr[i] is not None:
                node = cls(arr[i])
                curr_node.right = node
                node_queue.append(node)
            i += 1
        return root
