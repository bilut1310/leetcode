import ctypes
import os

OUT_DIR = 'out'
CC = 'gcc'


def clib(c_fname):
    basename = os.path.basename(c_fname)
    basename = os.path.splitext(basename)[0]
    so_fname = f'{OUT_DIR}/{basename}.so'

    if not os.path.isfile(so_fname) or os.path.getmtime(so_fname) < os.path.getmtime(c_fname):
        os.system(f'{CC} -shared -o {so_fname} {c_fname}')

    return ctypes.cdll.LoadLibrary(so_fname)
