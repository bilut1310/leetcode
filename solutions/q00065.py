# 65. Valid Number
# https://leetcode.com/problems/valid-number/


class Solution:
    def isNumber(self, s: str) -> bool:
        try:
            num = float(s)
        except ValueError:
            return False
        return True
