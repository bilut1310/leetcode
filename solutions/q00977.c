// 977. Squares of a Sorted Array
// https://leetcode.com/problems/squares-of-a-sorted-array/

#include <stdio.h>
#include <stdlib.h>

int *sortedSquares(int *A, int ASize, int *returnSize)
{
    int *result = malloc(ASize * sizeof(int));
    *returnSize = ASize;
    int i = 0, j = ASize - 1;

    for (int p = j; p > 0; p--)
    {
        if (-A[i] > A[j])
        {
            result[p] = A[i] * A[i];
            i++;
        }
        else
        {
            result[p] = A[j] * A[j];
            j--;
        }
    }
    result[0] = A[i] * A[i];
    return result;
}
