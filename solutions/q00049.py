# 49. Group Anagrams
# https://leetcode.com/problems/group-anagrams/

from typing import Dict, List


class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        result = {}  # type: Dict[str, List[str]]
        for s in strs:
            key = ''.join(sorted(s))
            if key in result:
                result[key].append(s)
            else:
                result[key] = [s]
        return list(result.values())
