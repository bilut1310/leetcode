# 15. 3Sum
# https://leetcode.com/problems/3sum/

from typing import List, Dict


class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        result = []  # type: List[List[int]]

        nums.sort()
        for i in range(0, len(nums) - 2):
            if nums[i] > 0:
                break

            if i != 0 and nums[i] == nums[i-1]:
                continue

            l = i + 1
            r = len(nums) - 1
            while r > l:
                s = nums[l] + nums[r] + nums[i]
                if s < 0:
                    l += 1
                elif s > 0:
                    r -= 1
                else:
                    result.append([nums[i], nums[l], nums[r]])
                    while l < r and nums[l+1] == nums[l]:
                        l += 1
                    while l < r and nums[r-1] == nums[r]:
                        r -= 1
                    l += 1
                    r -= 1

        return result
