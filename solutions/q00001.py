# 1. Two Sum
# https://leetcode.com/problems/two-sum/

from typing import List, Dict


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        hash_table = {}  # type: Dict[int, int]

        for i, n in enumerate(nums):
            if n not in hash_table:
                hash_table[target - n] = i
            else:
                return [hash_table[n], i]

        return []
