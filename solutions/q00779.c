// 779. K-th Symbol in Grammar
// https://leetcode.com/problems/k-th-symbol-in-grammar/

#include <stdio.h>
#include <stdlib.h>

int kthGrammar_recur(int N, int K)
{
    if (K == 1)
    {
        return 0;
    }

    // find k: k/2 < K <= k
    int n = 0, k = 1;
    for (; k < K; k *= 2, n++)
        ;

    return !kthGrammar(N - n, K - k / 2);
}

int kthGrammar(int N, int K)
{
    int k, result = 0;

    while (K != 1)
    {
        // find k: k/2 < K <= k
        for (k = 1; k < K; k *= 2)
            ;

        K -= k / 2;
        result = !result;
    }

    return result;
}
