// 658. Find K Closest Elements
// https://leetcode.com/problems/find-k-closest-elements/

#include <stdio.h>
#include <stdlib.h>

int *findClosestElements(int *arr, int arrSize, int k, int x, int *returnSize)
{
    *returnSize = k;
    int *result = malloc(k * sizeof(int));

    if (x <= arr[0])
    {
        for (int i = 0; i < k; i++)
        {
            result[i] = arr[i];
        }
    }
    else if (x >= arr[arrSize - 1])
    {
        for (int i = 0; i < k; i++)
        {
            result[i] = arr[i + arrSize - k];
        }
    }
    else
    {
        int i, l = 0, r = arrSize - 1;

        while (l != r)
        {
            i = (l + r) / 2;
            if (arr[i] > x)
            {
                r = i;
            }
            else if (arr[i] < x)
            {
                l = i;
            }
            else
            {
                l = r = i;
                break;
            }
            if (r - l == 1)
            {
                l = r;
                break;
            }
        }
        l -= 1;

        for (i = 0; i < k; i++)
        {
            if (l == -1)
            {
                break;
            }
            if (r == arrSize)
            {
                l -= k - i;
                break;
            }

            if (arr[r] - x < x - arr[l])
            {
                r += 1;
            }
            else
            {
                l -= 1;
            }
        }

        for (i = 0; i < k; i++)
        {
            result[i] = arr[l + i + 1];
        }
    }

    return result;
}
