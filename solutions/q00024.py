# 24. Swap Nodes in Pairs
# https://leetcode.com/problems/swap-nodes-in-pairs/

from common.data_structures import ListNode

class Solution:
    def swapPairs(self, head: ListNode) -> ListNode:
        if not head:
            return head

        cnode = head
        nnode = head.next
        if nnode is None:
            return head
        cnode.next = nnode.next
        nnode.next = cnode
        head = nnode

        while cnode.next is not None:
            pnode = cnode
            cnode = cnode.next
            nnode = cnode.next
            if nnode is None:
                return head
            cnode.next = nnode.next
            pnode.next = nnode
            nnode.next = cnode
        return head
