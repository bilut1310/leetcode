# 8. String to Integer (atoi)
# https://leetcode.com/problems/string-to-integer-atoi/


class Solution:
    def myAtoi(self, string: str) -> int:
        num = 0
        m = False
        if not string:
            return 0
        while string[0] == ' ':
            string = string[1:]
        if string[0] == '+':
            string = string[1:]
        elif string[0] == '-':
            string = string[1:]
            m = True
        for s in string:
            if s in '0123456789':
                num = num * 10 + int(s)
            else:
                break
        if m:
            if num > 2147483648:
                return -2147483648
            return -num
        else:
            if num > 2147483647:
                return 2147483647
            return num
