# 41. First Missing Positive
# https://leetcode.com/problems/first-missing-positive/

from typing import List


class Solution:
    def firstMissingPositive(self, nums: List[int]) -> int:
        size = len(nums)

        for i in range(size):
            while 0 < (n:= nums[i]) <= size:
                if n - 1 < i:
                    nums[n - 1] = n
                    break

                if n != nums[n - 1]:
                    nums[i] = nums[n - 1]
                    nums[n - 1] = n
                else:
                    break

        for i in range(size):
            if nums[i] != i + 1:
                return i + 1

        return size + 1
