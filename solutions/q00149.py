# 149. Max Points on a Line
# https://leetcode.com/problems/max-points-on-a-line/

from typing import List


class Solution:
    def maxPoints(self, points: List[List[int]]) -> int:
        def in_line(ps: List[List[int]], p_idx: int, line: List[int]) -> bool:
            i, j = line[:2]
            a = ps[i]
            b = ps[j]
            p = ps[p_idx]
            if a[0] == b[0]:
                return p[0] == a[0]
            elif a[1] == b[1]:
                return p[1] == a[1]
            else:
                x = (p[0] - a[0]) / (b[0] - a[0])
                y = (p[1] - a[1]) / (b[1] - a[1])
                return x == y

        ps = []  # type: List[List[int]]
        weight = []  # type: List[int]
        lines = []  # type: List[List[int]]
        for p in points:
            if p not in ps:
                ps.append(p)
                weight.append(1)
            else:
                weight[ps.index(p)] += 1
        num = len(ps)
        if num <= 2:
            return sum(weight)

        for i in range(num):
            for j in range(i + 1, num):
                find_line = False
                for line in lines:
                    if i in line and j not in line and in_line(ps, j, line):
                        line.append(j)
                        find_line = True
                if not find_line:
                    lines.append([i, j])

        return max((sum(weight[i] for i in l) for l in lines))
