# 454. 4Sum II
# https://leetcode.com/problems/4sum-ii/

from typing import List, Dict


class Solution:
    def fourSumCount(self, A: List[int], B: List[int], C: List[int], D: List[int]) -> int:
        ab_hash_table = {}  # type: Dict[int, int]
        result = 0

        for a in A:
            for b in B:
                s = -(a + b)
                if s in ab_hash_table:
                    ab_hash_table[s] += 1
                else:
                    ab_hash_table[s] = 1

        for c in C:
            for d in D:
                s = c + d
                if s in ab_hash_table:
                    result += ab_hash_table[s]

        return result
