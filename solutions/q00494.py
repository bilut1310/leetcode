# 494. Target Sum
# https://leetcode.com/problems/target-sum/

from typing import List, Dict


class Solution:
    def findTargetSumWays(self, nums: List[int], S: int) -> int:
        total = sum(nums)
        if total < S or (total + S) % 2 == 1:
            return 0

        total = (total - S) // 2
        dp = [0] * (total + 1)
        dp[0] = 1
        for n in nums:
            for i in range(total, n-1, -1):
                dp[i] += dp[i-n]

        return dp[-1]


class Solution2:
    def findTargetSumWays(self, nums: List[int], S: int) -> int:
        def binomial(n, r):
            p = 1
            for i in range(1, min(r, n - r) + 1):
                p *= n
                p //= i
                n -= 1
            return p

        num_counts = {}  # type: Dict[int, int]
        for n in nums:
            if n in num_counts:
                num_counts[n] += 1
            else:
                num_counts[n] = 1

        comb_count_list = []  # type: List[Dict[int, int]]
        for n, count in num_counts.items():
            if n == 0:
                comb_count_list.append({0: 2**count})
                continue

            comb_count = {}  # type: Dict[int, int]
            if count % 2 == 0:
                for i in range(0, count // 2):
                    x = (count - i * 2) * n
                    comb_count[x] = comb_count[-x] = binomial(count, i)
                comb_count[0] = binomial(count, count // 2)
            else:
                for i in range(0, count // 2 + 1):
                    x = (count - i * 2) * n
                    comb_count[x] = comb_count[-x] = binomial(count, i)
                pass

            comb_count_list.append(comb_count)

        while len(comb_count_list) > 1:
            comb1 = comb_count_list.pop()
            comb2 = comb_count_list.pop()
            comb = {}  # type: Dict[int, int]
            for n1, c1 in comb1.items():
                for n2, c2 in comb2.items():
                    n = n1 + n2
                    if n in comb:
                        comb[n] += c1 * c2
                    else:
                        comb[n] = c1 * c2

            comb_count_list.append(comb)

        comb = comb_count_list[0]
        if S in comb:
            return comb[S]
        else:
            return 0
