# 915. Partition Array into Disjoint Intervals
# https://leetcode.com/problems/partition-array-into-disjoint-intervals/

from typing import List


class Solution:
    def partitionDisjoint(self, A: List[int]) -> int:
        max_left = A[0]
        max_seen = A[0]
        result = 0

        for i in range(1, len(A)):
            if A[i] > max_seen:
                max_seen = A[i]
            elif A[i] < max_left:
                result = i
                max_left = max_seen

        return result + 1
