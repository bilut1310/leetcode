# 45. Jump Game II
# https://leetcode.com/problems/jump-game-ii/

from typing import List, Tuple


class Solution:
    def jump(self, nums: List[int]) -> int:
        if len(nums) == 1:
            return 0

        result = 1
        curr_pos = 0
        next_pos = 0

        for i, n in enumerate(nums):
            if i > curr_pos:
                result += 1
                curr_pos = next_pos

            next_pos = max((next_pos, i + n))
            if next_pos >= len(nums) - 1:
                return result
        return result
