# 376. Wiggle Subsequence
# https://leetcode.com/problems/wiggle-subsequence/

from typing import List


class Solution:
    def wiggleMaxLength(self, nums: List[int]) -> int:
        if len(nums) < 2:
            return len(nums)

        length = 1
        status = 0  # 0: init, 1: delta > 0, -1: delta < 0

        for i in range(1, len(nums)):
            if nums[i] > nums[i - 1] and status <= 0:
                length += 1
                status = 1
            elif nums[i] < nums[i - 1] and status >= 0:
                length += 1
                status = -1

        return length
