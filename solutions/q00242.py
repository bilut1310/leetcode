# 242. Valid Anagram
# https://leetcode.com/problems/valid-anagram/


class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        # return ''.join(sorted(s)) == ''.join(sorted(t))
        if len(s) != len(t):
            return False

        for ss in set(s):
            if s.count(ss) != t.count(ss):
                return False

        return True
