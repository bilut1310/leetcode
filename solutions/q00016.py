# 16. 3Sum Closest
# https://leetcode.com/problems/3sum-closest/

import sys
from typing import List


class Solution:
    def threeSumClosest(self, nums: List[int], target: int) -> int:
        closest = sys.maxsize
        min_delta = sys.maxsize
        size = len(nums)

        nums.sort()
        for i in range(0, size - 2):
            if i != 0 and nums[i] == nums[i - 1]:
                continue

            s = nums[i] + nums[i + 1] + nums[i + 2]
            if s > target:
                return s if abs(target - s) < min_delta else closest

            s = nums[i] + nums[size - 1] + nums[size - 2]
            if s < target:
                d = abs(target - s)
                if d < min_delta:
                    closest = s
                    min_delta = d
                continue

            l = i + 1
            r = size - 1
            while r > l:
                s = nums[l] + nums[r] + nums[i]
                d = abs(target - s)
                if d < min_delta:
                    closest = s
                    min_delta = d

                if s < target:
                    l += 1
                elif s > target:
                    r -= 1
                else:
                    return target

        return closest
