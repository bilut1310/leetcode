# 665. Non-decreasing Array
# https://leetcode.com/problems/non-decreasing-array/

from typing import List


class Solution:
    def checkPossibility(self, nums: List[int]) -> bool:
        n = len(nums)
        if len(nums) <= 2:
            return True

        p = -1

        if nums[0] > nums[1]:
            p = 0
        if nums[1] > nums[2]:
            if p == -1:
                p = 1
            else:
                return False

        for i in range(2, n - 1):
            if nums[i] > nums[i + 1]:
                if p == -1:
                    p = i
                else:
                    return False
            elif p == i - 1 and nums[i - 2] > nums[i] and nums[i - 1] > nums[i + 1]:
                return False

        return True
