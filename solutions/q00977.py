# 977. Squares of a Sorted Array
# https://leetcode.com/problems/squares-of-a-sorted-array/

from typing import List


class Solution:
    def sortedSquares(self, A: List[int]) -> List[int]:
        if not A:
            return A

        if A[0] >= 0:
            return list(x*x for x in A)
        if A[-1] <= 0:
            return list(x*x for x in A[::-1])

        result = [0] * len(A)
        i, j = 0, len(A)-1
        for p in range(j, i, -1):
            if -A[i] > A[j]:
                result[p] = A[i] * A[i]
                i += 1
            else:
                result[p] = A[j] * A[j]
                j -= 1
        result[0] = A[i] * A[i]
        return result
