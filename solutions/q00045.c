// 45. Jump Game II
// https://leetcode.com/problems/jump-game-ii/

#include <stdio.h>
#include <stdlib.h>

int jump(int *nums, int numsSize)
{
    if (numsSize == 1)
    {
        return 0;
    }

    int result = 1;
    int pos = 0;
    int curr_pos = 0;
    int next_pos = 0;

    for (int i = 0; i < numsSize; i++)
    {
        if (i > curr_pos)
        {
            result++;
            curr_pos = next_pos;
        }

        pos = i + nums[i];
        next_pos = pos > next_pos ? pos : next_pos;
        if (next_pos >= numsSize - 1)
        {
            return result;
        }
    }

    return result;
}
