import pytest
from common.data_structures import ListNode
from solutions.q00024 import Solution


testdata = [
    ('1->2->3->4', '2->1->4->3'),
    ('1->2->3->4->5->6->7->8', '2->1->4->3->6->5->8->7'),
    ('1->2', '2->1'),
]

sol = Solution()


@pytest.mark.parametrize('head,ans', testdata)
def test_solution(head, ans):
    out = sol.swapPairs(ListNode.from_str(head))
    out = str(out)
    assert out == ans
