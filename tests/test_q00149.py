import pytest
from solutions.q00149 import Solution


testdata = [
    ([[1, 1], [2, 2], [3, 3]], 3),
    ([[1, 1], [3, 2], [5, 3], [4, 1], [2, 3], [1, 4]], 4),
    ([], 0),
    ([[0, 0]], 1),
    ([[0, 0], [1, 1], [0, 0]], 3),
    ([[1, 1], [1, 1], [2, 3]], 3),
    ([[1, 1], [1, 1], [1, 1]], 3),
]

sol = Solution()


@pytest.mark.parametrize('nums,ans', testdata)
def test_solution(nums, ans):
    out = sol.maxPoints(nums)
    assert out == ans
