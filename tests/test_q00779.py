import pytest
from ctypes import c_int
from common.clib import clib

testdata = [
    (1, 1, 0),
    (2, 1, 0),
    (2, 2, 1),
    (4, 5, 1),
]

# Solution in C
sol_c = clib('solutions/q00779.c').kthGrammar
sol_c.argtypes = [c_int, c_int]
sol_c.restype = c_int


@pytest.mark.parametrize('N,K,ans', testdata)
def test_solution_in_c(N, K, ans):
    out = sol_c(N, K)
    assert out == ans
