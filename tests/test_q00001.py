import pytest
from solutions.q00001 import Solution


testdata = [
    ([2, 7, 11, 15], 9, [0, 1]),
]

sol = Solution()


@pytest.mark.parametrize('nums,target,ans', testdata)
def test_solution(nums, target, ans):
    out = sol.twoSum(nums, target)
    assert out == ans
