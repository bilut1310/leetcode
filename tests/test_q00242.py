import pytest
from solutions.q00242 import Solution


testdata = [
    ('anagram', 'nagaram', True),
    ('rat', 'car', False),
    ('a', 'ab', False),
]

sol = Solution()


@pytest.mark.parametrize('s,t,ans', testdata)
def test_solution(s, t, ans):
    out = sol.isAnagram(s, t)
    assert out == ans
