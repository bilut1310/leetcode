import pytest
from ctypes import c_int, POINTER
from common.clib import clib

testdata = [
    ([1, 2, 3, 4, 5], 4, 3, [1, 2, 3, 4]),
    ([1, 2, 3, 4, 5], 4, -1, [1, 2, 3, 4]),
]

# Solution in C
_sol_c = clib('solutions/q00658.c').findClosestElements
_sol_c.argtypes = [POINTER(c_int), c_int, c_int, c_int, POINTER(c_int)]
_sol_c.restype = POINTER(c_int)


def sol_c(arr, k, x):
    arr = (c_int * len(arr))(*arr)
    returnSize = c_int()
    out = _sol_c(arr, len(arr), k, x, returnSize)
    return [out[i] for i in range(returnSize.value)]


@pytest.mark.parametrize('arr,k,x,ans', testdata)
def test_solution_in_c(arr, k, x, ans):
    out = sol_c(arr, k, x)
    assert out == ans
