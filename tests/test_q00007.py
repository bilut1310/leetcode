import pytest
from solutions.q00007 import Solution


testdata = [
    (123, 321),
    (-123, -321),
    (120, 21),
    (1534236469, 0),
]

sol = Solution()


@pytest.mark.parametrize('x,ans', testdata)
def test_solution(x, ans):
    out = sol.reverse(x)
    assert out == ans
