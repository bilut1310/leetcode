import pytest
from solutions.q00041 import Solution


testdata = [
    ([1, 2, 0], 3),
    ([3, 4, -1, 1], 2),
    ([7, 8, 9, 11, 12], 1),
    ([], 1),
    ([0], 1),
    ([1], 2),
    ([1, 2], 3),
    ([2, 2], 1),
    ([2, 1], 3),
]

sol = Solution()


@pytest.mark.parametrize('nums,ans', testdata)
def test_solution(nums, ans):
    out = sol.firstMissingPositive(nums)
    assert out == ans
