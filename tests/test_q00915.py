import pytest
from solutions.q00915 import Solution


testdata = [
    ([5,0,3,8,6], 3),
    ([1,1,1,0,6,12], 4),
]

sol = Solution()


@pytest.mark.parametrize('A,ans', testdata)
def test_solution(A, ans):
    out = sol.partitionDisjoint(A)
    assert out == ans
