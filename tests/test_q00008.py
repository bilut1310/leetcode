import pytest
from solutions.q00008 import Solution


testdata = [
    ('42', 42),
    ('   -42', -42),
    ('4193 with words', 4193),
    ('words and 987', 0),
    ('-91283472332', -2147483648),
]

sol = Solution()


@pytest.mark.parametrize('string,ans', testdata)
def test_solution(string, ans):
    out = sol.myAtoi(string)
    assert out == ans
