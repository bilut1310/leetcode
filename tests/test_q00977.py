import pytest
from solutions.q00977 import Solution
from ctypes import c_int, POINTER
from common.clib import clib

testdata = [
    [-4, -1, 0, 3, 10],
    [-7, -3, 2, 3, 11],
    [1, 2, 3, 4, 5],
    [0, 0, 1, 2, 3, 4, 5],
    [0, 0, 1, 1, 1, 1, 2, 3, 4, 5, 6, 6],
    [-6, -5, -4, -1],
    [-6, -5, -4, -1, -1],
    [-6, -5, -4, -1, -1, 0, 0],
    [1, 1, 1, 1],
    [-2, -2, -2],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [],
    [-1, 1, 2],
    [-1, -1, -1, 2],
    [-1, 1],
    [-3, -3, -2, 1],
]

sol = Solution().sortedSquares

# Solution in C
_sol_c = clib('solutions/q00977.c').sortedSquares
_sol_c.argtypes = [POINTER(c_int), c_int, POINTER(c_int)]
_sol_c.restype = POINTER(c_int)

def sol_c(A):
    A = (c_int * len(A))(*A)
    returnSize = c_int()
    out = _sol_c(A, len(A), returnSize)
    return [out[i] for i in range(returnSize.value)]


@pytest.mark.parametrize('A', testdata)
def test_solution(A):
    ans = sorted(x * x for x in A)
    out = sol(A)
    assert out == ans


@pytest.mark.parametrize('A', testdata)
def test_solution_in_c(A):
    ans = sorted(x * x for x in A)
    out = sol_c(A)
    assert out == ans
