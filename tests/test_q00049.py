import pytest
from solutions.q00049 import Solution


testdata = [
    (
        ['eat', 'tea', 'tan', 'ate', 'nat', 'bat'],
        [['bat'], ['nat', 'tan'], ['ate', 'eat', 'tea']]
    ),
    (
        ['hos','boo','nay','deb','wow','bop','bob','brr','hey','rye','eve','elf','pup','bum','iva','lyx','yap','ugh','hem','rod','aha','nam','gap','yea','doc','pen','job','dis','max','oho','jed','lye','ram','pup','qua','ugh','mir','nap','deb','hog','let','gym','bye','lon','aft','eel','sol','jab'],
        [['eel'],['aft'],['lon'],['bye'],['gym'],['let'],['hog'],['mir'],['iva'],['brr'],['eve'],['nay'],['sol'],['pup','pup'],['max'],['bum'],['lye'],['gap'],['hey'],['boo'],['aha'],['elf'],['bob'],['hem'],['doc'],['oho'],['wow'],['deb','deb'],['hos'],['rye'],['bop'],['yap'],['ugh','ugh'],['ram'],['rod'],['nam'],['yea'],['nap'],['pen'],['job'],['lyx'],['dis'],['jed'],['jab'],['qua']]
    )
]

sol = Solution()


@pytest.mark.parametrize('strs,ans', testdata)
def test_solution(strs, ans):
    out = sol.groupAnagrams(strs)
    out = sorted(sorted(o) for o in out)
    ans = sorted(sorted(a) for a in ans)
    assert out == ans
