import pytest
from solutions.q00454 import Solution


testdata = [
    ([ 1, 2], [-2,-1], [-1, 2], [ 0, 2], 2)
]

sol = Solution()


@pytest.mark.parametrize('A,B,C,D,ans', testdata)
def test_solution(A, B, C, D, ans):
    out = sol.fourSumCount(A, B, C, D)
    assert out == ans
