import pytest
from solutions.q00665 import Solution


testdata = [
    ([4, 2, 3], True),
    ([4, 2, 1], False),
    ([4, 2, 3, 1], False),
    ([4, 2, 3, 5], True),
    ([], True),
    ([1], True),
    ([-1, 4, 2, 3], True),
    ([-1, 4, 5, 2, 3], False),
    ([-1, 4, -1, 2, 3], True),
    ([-1, 4, 3, 2, 3], False),
    ([1, 1, 1], True),
    ([1, 2, 5, 3, 3], True),
    ([1, 2, 3, 3, 2, 4], True),
    ([1, 2, 3, 4, 9, 6, 6, 7], True),
]

sol = Solution()


@pytest.mark.parametrize('nums,ans', testdata)
def test_solution(nums, ans):
    out = sol.checkPossibility(nums)
    assert out == ans
