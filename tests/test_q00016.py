import pytest
from solutions.q00016 import Solution


testdata = [
    ([-1, 2, 1, -4], 1, 2),
    ([-1, 2, 1, 1, -4], 1, 1),
    ([-1, 2, 1, 1, 1, -4], 1, 1),
]

sol = Solution()


@pytest.mark.parametrize('nums,target,ans', testdata)
def test_solution(nums, target, ans):
    out = sol.threeSumClosest(nums, target)
    assert out == ans
