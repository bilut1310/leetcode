import pytest
from solutions.q00017 import Solution


testdata = [
    ('23', ['ad', 'ae', 'af', 'bd', 'be', 'bf', 'cd', 'ce', 'cf']),
    ('7', ['p','q','r','s']),
]

sol = Solution()


@pytest.mark.parametrize('digits,ans', testdata)
def test_solution(digits, ans):
    out = sol.letterCombinations(digits)
    out = sorted(out)
    assert out == ans
