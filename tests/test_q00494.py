import pytest
from solutions.q00494 import Solution


testdata = [
    ([1, 1, 1, 1, 1], 3, 5),
    ([1,1,1,1,1,2,2,2,2], 3, 71),
    ([1,1,1,1,1,2,2,2,2,3,3,3,4,4,5], 3, 2374),
    ([1], 2, 0),
    ([1,0], 1, 2)
]

sol = Solution()


@pytest.mark.parametrize('nums,S,ans', testdata)
def test_solution(nums, S, ans):
    out = sol.findTargetSumWays(nums, S)
    assert out == ans
