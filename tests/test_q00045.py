import pytest
from solutions.q00045 import Solution
from ctypes import c_int, POINTER
from common.clib import clib

testdata = [
    ([2, 3, 1, 1, 4], 2),
    ([0], 0),
    ([4, 5], 1),
    ([10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 1, 0], 2),
    ([7, 0, 9, 6, 9, 6, 1, 7, 9, 0, 1, 2, 9, 0, 3], 2),
    ([10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 1, 0], 2),
]

sol = Solution().jump

# Solution in C
_sol_c = clib('solutions/q00045.c').jump
_sol_c.argtypes = [POINTER(c_int), c_int]
_sol_c.restype = c_int

def sol_c(nums):
    nums = (c_int * len(nums))(*nums)
    out = _sol_c(nums, len(nums))
    return out


@pytest.mark.parametrize('nums,ans', testdata)
def test_solution(nums, ans):
    out = sol(nums)
    assert out == ans


@pytest.mark.parametrize('nums,ans', testdata)
def test_solution_in_c(nums, ans):
    out = sol_c(nums)
    assert out == ans
